﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data.Entity;

namespace passionproject.Models
{
    public class playerrecords
    {
        public int ID { get; set; }
        public string PlayerName { get; set; }
        public int Age { get; set; }
        public int Matches { get; set; }
        public int Runs { get; set; }
    }
    public class PlayersDB : DbContext
    {
        public DbSet<playerrecords> passionproject { get; set; }
    }


}