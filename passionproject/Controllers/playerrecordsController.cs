﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using passionproject.Models;

namespace passionproject.Controllers
{
    public class playerrecordsController : Controller
    {
        private PlayersDB db = new PlayersDB();

        
        public ActionResult Index()
        {
            return View(db.passionproject.ToList());
        }

        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            playerrecords playerrecords = db.passionproject.Find(id);
            if (playerrecords == null)
            {
                return HttpNotFound();
            }
            return View(playerrecords);
        }

        
        public ActionResult Create()
        {
            return View();
        }

        
        
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PlayerName,Age,Matches,Runs")] playerrecords playerrecords)
        {
            if (ModelState.IsValid)
            {
                db.passionproject.Add(playerrecords);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(playerrecords);
        }

        
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            playerrecords playerrecords = db.passionproject.Find(id);
            if (playerrecords == null)
            {
                return HttpNotFound();
            }
            return View(playerrecords);
        }

        // POST: playerrecords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PlayerName,Age,Matches,Runs")] playerrecords playerrecords)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playerrecords).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playerrecords);
        }

        // GET: playerrecords/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            playerrecords playerrecords = db.passionproject.Find(id);
            if (playerrecords == null)
            {
                return HttpNotFound();
            }
            return View(playerrecords);
        }

        // POST: playerrecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            playerrecords playerrecords = db.passionproject.Find(id);
            db.passionproject.Remove(playerrecords);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
